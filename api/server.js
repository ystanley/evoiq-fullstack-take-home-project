const express = require('express');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();

const app = express();

// Global sqlite db connection
let db = new sqlite3.Database('./api/db/sample.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    throw err;
  }
  console.log('Connected to the sqlite database.');
});

// Health test
app.get('/api/health', function (req, res) {
 return res.send('OK');
});

// Get all team members from Members table
app.get('/api/members', function (req, res) {
  let sql = 'SELECT name, team, role FROM Members';
  db.all(sql, [], (err, rows) => {
    if (err) {
      throw err;
    }
    return res.json({members: rows});
  });
});

app.listen(8080);
