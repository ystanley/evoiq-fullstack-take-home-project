# Fullstack Take Home Project

This project contains a simple React app and Express API server.

## Setup

1. Install Yarn by following the instructions for your operating system here: https://classic.yarnpkg.com/en/docs/install/.
2. Clone this repository.
3. Go into the project's root directory and run `yarn install` to install all necessary packages. On OSX, you may see some error about `gyp` and Xcode. That error is safe to ignore.
4. Open a new terminal and run `yarn start-api`. Verify there are no errors.
5. Open another terminal and run `yarn start`. Verify there are no errors.


## Project Details

### Directory Structure
The `api` directory contains a basic Express server and a SQLite database file.

The `src` directory contains all React UI code.

### Workflow
The recommended workflow is to have run `yarn start-api` and `yarn start` in two different terminals. This will monitor changes to the API server and client code, respectively.

The API server will always host on `localhost:8080` and the client should host on `localhost:3000`.

### Architecture Details

The Express server is a single `server.js` file.

The SQLite database contains a single table `Members` and is prepopulated with three rows.

The React code is the automatically generate via [Create React App](https://create-react-app.dev/docs/getting-started/)

## Objective

Create a table to display all team members information. Please commit when appropriate. Your commit history will be reviewed.

We recommend using react-table but feel free to use another framework or no framework.

### Requirements
- On page load, the table data is fetched from the API server.
- Rows have a display state and edit state.
- Users can enter the edit state by clicking on some edit button or icon.
- All columns values are editable: Name is a text input. Teams and Roles are dropdowns.
- Users can save their changes and it should persist in the database.
- Users should be able to cancel their changes.
- Each column header should sort on click.
- Users can hide and show columns
- Appropriate unit tests
- Basic styling. See sample UI below

### Bonus Features

If you are itching to implement more features, here are some ideas that we believe are interesting to implement.  
To be clear, these features are not required to complete this assignment.

- Add User
- Client side pagination


### Sample UI

Here is a sample UI for styling inspiration. Feel free to style the table in your own way!


![image](https://gitlab.com/evolutioniq/ark/uploads/ed27fe7070186108c2b3b2a1cb98d066/image.png)
